const fs = require("fs");
const { execSync } = require("child_process");
const path = require("path");

const cwd = process.cwd();

const pkg = require(path.resolve(cwd, "package.json"));

const versionInfo = { version: `${pkg.version}` };

const versionFileName = path.resolve(cwd, "version.json");

fs.writeFileSync(versionFileName, JSON.stringify(versionInfo, null, 2));

execSync(`git add ${versionFileName}`);
