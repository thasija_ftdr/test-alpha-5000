/**
 * This is a jest setup file to hold common test setup code that should run
 * before each test suite. This is configured using the `setupFilesAfterEnv`
 * property in in project's `jest.config.js`.
 *
 * @see {@link https://jestjs.io/docs/en/configuration#setupfilesafterenv-array|Rollup Plugins}
 */

// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
// E.g. import "@testing-library/jest-dom/extend-expect";
