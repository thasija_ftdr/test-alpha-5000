# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.11.0](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-1@0.10.0...@ftdr/example-multi-package-1@0.11.0) (2021-10-14)


### Features ✨

* add version gathering script to packate-1 ([41ebe0d](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/41ebe0d39c08a6817ebc75fcf5d96ec5ea8a69bd))


### Bug Fixes 🐛

* update verion gen script ([b69ca0f](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/b69ca0f07548f77e2691450f115c1668dd637cfb))



## [0.10.0](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-1@0.9.0...@ftdr/example-multi-package-1@0.10.0) (2021-05-17)


### Bug Fixes 🐛

* **package-1:** dummy fix ([1c9a313](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/1c9a3136cd0eeda1eb1b9171c3faad218c596c09))
* **package-1:** dummy fix commit ([89fce17](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/89fce17e74fc6695d625f5c9d8444bbfb0ec638b))


### Features ✨

* **package-1:** dummy feature ([4096459](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/40964591506e0a69d9de71f7d97c11d0479d37ed))



## [0.9.0](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-1@0.9.0-rc.1...@ftdr/example-multi-package-1@0.9.0) (2021-05-14)

**Note:** Version bump only for package @ftdr/example-multi-package-1





## [0.9.0-rc.1](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-1@0.9.0-rc.0...@ftdr/example-multi-package-1@0.9.0-rc.1) (2021-05-14)


### Bug Fixes 🐛

* **package-1:** another dummy fix to force bump patch version ([0fd6f17](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/0fd6f175b40de80b042a042bdf6441b068aa3cfe))



## [0.9.0-rc.0](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-1@0.9.0-alpha.0...@ftdr/example-multi-package-1@0.9.0-rc.0) (2021-05-14)


### Bug Fixes 🐛

* **package-1:** dummy fix to force bump patch version ([6f08b84](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/6f08b84f8740480543f96c147d4152cf9384dce0))



## [0.9.0-alpha.0](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-1@0.8.0...@ftdr/example-multi-package-1@0.9.0-alpha.0) (2021-05-14)


### Features ✨

* **package-1:** dummy feature to force release ([4c2ced2](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/4c2ced291a48de114d90f496e4f4a1ac4c6f5f72))



## [0.8.0](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-1@0.7.1...@ftdr/example-multi-package-1@0.8.0) (2021-03-30)


### Features ✨

* dummy change to force release ([e1082ff](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/e1082ffb078bae5723de524a2a6cdb8023996546))



### [0.7.1](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-1@0.7.0...@ftdr/example-multi-package-1@0.7.1) (2021-03-25)


### Bug Fixes 🐛

* dummy change to force release ([faa835c](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/faa835ce0c963ed5766f2f4ec9f7b9a2df2fa51d))



## [0.7.0](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-1@0.6.0...@ftdr/example-multi-package-1@0.7.0) (2021-03-24)


### Features ✨

* here we go again ([fe764c1](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/fe764c193bb0aefce5e0470c420983c672cbd80f))



## [0.6.0](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-1@0.5.1...@ftdr/example-multi-package-1@0.6.0) (2021-03-24)


### Features ✨

* **package-1:** dummy chage to force release ([95ff288](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/95ff28884e3e2312ecac978f49e3738fbf8bd3f2))



### [0.5.1](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-1@0.5.0...@ftdr/example-multi-package-1@0.5.1) (2021-03-24)


### Bug Fixes 🐛

* **package-1:** dummy change to force release ([7f89e8c](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/7f89e8cfe007e4b82ea3345cc16265ef97246441))



## [0.5.0](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-1@0.4.0...@ftdr/example-multi-package-1@0.5.0) (2021-03-24)


### Features ✨

* **package-1:** dummy change to force a release ([4931083](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/4931083cca4c2793e8468a9ef8b6ab908c2f105d))



## [0.4.0](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-1@0.3.0...@ftdr/example-multi-package-1@0.4.0) (2021-03-24)


### Features ✨

* **package-1:** dummy change to force pipeline ([9e5e12c](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/9e5e12c773145d1fe923e4801bf0d45c58ec20b0))
* **package-1:** dummy change to trigger release ([f05eb8b](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/f05eb8b5b77145f675140b3e8429330b72b5dc34))



## [0.3.0](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-1@0.2.1...@ftdr/example-multi-package-1@0.3.0) (2021-03-24)


### Features ✨

* **package-1:** new dummy feature ([90bc155](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/90bc155fc835cade593ece4f5c513942b1a69169))



### [0.2.1](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-1@0.2.0...@ftdr/example-multi-package-1@0.2.1) (2021-03-24)


### Bug Fixes 🐛

* dummy change to trigger release ([1392a3f](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/1392a3f8246d0c009a18c000afe6e961dc899fc1))



## 0.2.0 (2021-03-24)


### Bug Fixes 🐛

* **package-1:** dummy change to force publish ([7f58a8b](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/7f58a8bb9ef7f30a2ee6d915ab4dd7d4e34e97d5))
* add npmrc config for non-master prepare stage ([64ed847](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/64ed847394f917ca34d0fc0c9af9a55237f06c81))


### Features ✨

* dummy feature ([5545600](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/554560088f86bab992f96e30d8228f6aad2282ee))



## 0.1.0 (2021-03-23)


### ⚠ BREAKING CHANGES

* initial project setup

### Features ✨

* initial project setup ([145047f](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/145047ffbe87b09297a9b449d3fcee0d8b745f8b))
