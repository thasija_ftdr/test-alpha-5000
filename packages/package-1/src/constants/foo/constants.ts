export const CSS_CLASSES = {
  PI: 3.1415,
  ROOT: "css-root",
  CONTAINER: "css-container",
  WRAPPER: "css-wrapper",
  BOX: "css-box",
};
