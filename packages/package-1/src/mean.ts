import _mean from "lodash/mean";
import { CSS_CLASSES } from "./constants/foo/constants";
import values from "./constants/values.json";

/**
 * Calculates arithmetic mean.
 *
 * Some more description here.!!
 *
 * @param numbers A list of numbers to calculate mean
 */
export function mean(numbers: number[]): number {
  if (process.env.NODE_ENV === "development") {
    console.log(`Received args: ${numbers}`);
  }

  console.log(values);

  const myString = [
    CSS_CLASSES.BOX,
    CSS_CLASSES.CONTAINER,
    CSS_CLASSES.ROOT,
    CSS_CLASSES.WRAPPER,
  ].join(", ");
  console.log(myString);

  const myPi = CSS_CLASSES.PI;
  console.log(myPi);

  return _mean(numbers);
}
