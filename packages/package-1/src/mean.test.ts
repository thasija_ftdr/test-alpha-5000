import { mean } from "./mean";

describe("add()", () => {
  test("should return correct result", () => {
    expect(mean([1, 2, 3])).toEqual(2);
  });
});
