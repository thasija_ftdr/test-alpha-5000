import babel from "@rollup/plugin-babel";
import commonjs from "@rollup/plugin-commonjs";
import json from "@rollup/plugin-json";
import resolve from "@rollup/plugin-node-resolve";
import replace from "@rollup/plugin-replace";
import externals from "rollup-plugin-node-externals";
import pkg from "./package.json";

const extensions = [".js", ".jsx", ".ts", ".tsx"];

const commonOutputOptions = {
  exports: "named",
  sourcemap: true,
  preserveModules: true,
  preserveModulesRoot: "src",
};

export default {
  input: "src/index.ts",
  plugins: [
    externals({ peerDeps: true, deps: true }),
    replace({
      "process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV),
      "preventAssignment": true,
    }),
    json(),
    resolve({
      browser: true,
      extensions,
    }),
    commonjs(),
    babel({
      rootMode: "upward",
      extensions,
      babelHelpers: "runtime",
      include: ["./src/**/*"],
    }),
  ],
  output: [
    {
      dir: pkg.main.replace(/\/index.js$/, ""),
      format: "cjs",
      ...commonOutputOptions,
    },
    {
      dir: pkg.module.replace(/\/index.js$/, ""),
      format: "es",
      ...commonOutputOptions,
    },
  ],
};
