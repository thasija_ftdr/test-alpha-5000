# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### [0.6.1](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-2@0.6.0...@ftdr/example-multi-package-2@0.6.1) (2021-05-17)


### Bug Fixes 🐛

* **package-2:** dummy fix ([9c56997](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/9c569976b41fd1559c1f7f8e5b205902e1fea81e))
* **package-2:** dummy fix ([24330b5](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/24330b576aa19b4ec03d3f4f3e9417e1e57c4fd1))



## [0.6.0](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-2@0.5.0...@ftdr/example-multi-package-2@0.6.0) (2021-03-30)


### Features ✨

* dummy change to force release ([e1082ff](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/e1082ffb078bae5723de524a2a6cdb8023996546))



## [0.5.0](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-2@0.4.1...@ftdr/example-multi-package-2@0.5.0) (2021-03-24)


### Features ✨

* here we go again ([fe764c1](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/fe764c193bb0aefce5e0470c420983c672cbd80f))



### [0.4.1](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-2@0.4.0...@ftdr/example-multi-package-2@0.4.1) (2021-03-24)


### Bug Fixes 🐛

* dummy change to trigger release ([b206bf5](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/b206bf5e9dbe593dd8f84e31adf885f0fc97ca09))



## [0.4.0](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-2@0.3.1...@ftdr/example-multi-package-2@0.4.0) (2021-03-24)


### Features ✨

* **package-2:** dummy change to force a release ([62db9c7](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/62db9c7fa77e06296f3db6503354c0a501324504))



### [0.3.1](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-2@0.3.0...@ftdr/example-multi-package-2@0.3.1) (2021-03-24)


### Bug Fixes 🐛

* dummy change to trigger release ([691b8d2](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/691b8d268a0e3dad324fbd930c883c24c7e149da))



## [0.3.0](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-2@0.2.1...@ftdr/example-multi-package-2@0.3.0) (2021-03-24)


### Features ✨

* **package-2:** dummy change to force a release ([76463e0](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/76463e0d7cb094419b7e868a2d28a9632db951d4))



### [0.2.1](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-2@0.2.0...@ftdr/example-multi-package-2@0.2.1) (2021-03-24)


### Bug Fixes 🐛

* **package-2:** dummy change to force release ([6f422e0](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/6f422e01cf929af15587d8f7f6dca3adbe177595))



## [0.2.0](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-2@0.1.3...@ftdr/example-multi-package-2@0.2.0) (2021-03-24)


### Features ✨

* **package-2:** new dummy feature ([a1d81c1](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/a1d81c13fcfcbb665cc16ef718eb03399307c445))



### [0.1.3](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-2@0.1.2...@ftdr/example-multi-package-2@0.1.3) (2021-03-24)


### Bug Fixes 🐛

* dummy change ([550f116](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/550f116095cbf63a4cb35705f0a874740eadc50a))



### [0.1.2](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/compare/@ftdr/example-multi-package-2@0.1.1...@ftdr/example-multi-package-2@0.1.2) (2021-03-24)


### Bug Fixes 🐛

* dummy change ([33c9975](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/33c9975ea37083139f2f2d90c922f1e45f829d5d))



### 0.1.1 (2021-03-24)


### Bug Fixes 🐛

* add npmrc config for non-master prepare stage ([64ed847](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/64ed847394f917ca34d0fc0c9af9a55237f06c81))



## 0.1.0 (2021-03-23)


### ⚠ BREAKING CHANGES

* initial project setup

### Features ✨

* **package-1:** a dummy change to force a pipeline run ([ea1a931](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/ea1a931feed8bd17254e833fcfc97d652540266a))
* initial project setup ([145047f](https://gitlab.com/ftdr/users/ftdr_avinash/examples/multi-package-ts-library/commit/145047ffbe87b09297a9b449d3fcee0d8b745f8b))
