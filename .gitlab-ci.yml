# This pipeline config is intended to be the standard pipeline setup for JS monorepos.
#
# Requirements:
# 1. Yarn is used for package management.
# 2. Lerna is used for monorepo management.
# 3. Each package in the monorepo has the following scripts in their package.json
#    lint, format, type-check, test, build
# 4. Each package generates it build artifacts in the `dist` sub-directory.
# 5. Each package generates test coverage reports in the `coverage` sub-directory.

image: node:15.4.0

default:
  tags:
    - ftdr-gcp

# Define a global cache that will be created per branch.
# Set it up to upload/download the `node_modules` directory.
# This cache will be used to allow subsequent jobs in the pipeline to re-use
# the same `node_modules` contents without needing to run `yarn install` again.
cache: &branch_cache
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - node_modules/

# Hidden job in a GitLab CI config file are jobs that start with a dot (.) character.
# These job are not it is not processed by GitLab CI/CD.
# Read more here: https://docs.gitlab.com/ee/ci/yaml/README.html#hide-jobs
#
# These jobs definitions can be used as templates for common, recurring job
# configuration.
#
# These templates (one or more) can then be merged in to other job definitions
# using the `extends` keyword. Note that this applied the entire set of config
# in these hidden jobs to the job that extends them.
# Read more here: https://docs.gitlab.com/ee/ci/yaml/README.html#extends
#
# In some cases it may be required to apply only portions of the template to
# corresponding sections of an extending job. In such cases use the `!reference`
# tag to do so.
# Read more here: https://docs.gitlab.com/ee/ci/yaml/README.html#reference-tags

.branch-cache-pusher-job:
  cache:
    <<: *branch_cache
    policy: push

.branch-cache-puller-job:
  cache:
    <<: *branch_cache
    policy: pull

.retrying-job:
  retry:
    max: 2

.npm-config-script-commands:
  script:
    - npm config set '@ftdr:registry=https://gitlab.com/api/v4/packages/npm/'
    - npm config set '//gitlab.com/api/v4/packages/npm/:_authToken' ${GITLAB_TOKEN}
    - npm config set '//gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken' ${GITLAB_TOKEN}
    - npm config set '//gitlab.com/api/v4/projects/:_authToken' ${GITLAB_TOKEN}

stages:
  - prepare
  - code-quality-check
  - test
  - build
  - release

# This job does two things:
# 1. Install package dependencies. The installed deps in `node_modules` directory
#    are then available for all subsequent jobs to re-use via the pipeline cache.
# 2. Identify the monorepo packages that actually changed. This list is written
#    to a file and uploaded as an artifact. All subsequent jobs can use this
#    artifact to restrict their action to only those packages that changed.
prepare:
  stage: prepare
  extends:
    - .branch-cache-pusher-job
    - .retrying-job
  script:
    - !reference [.npm-config-script-commands, script]
    - node -v
    - npm -v
    - yarn -v
    - yarn install --frozen-lockfile
    - npx lerna -v
    - node -e 'console.log(process.argv.slice(1).map(p => `--scope ${p}`).join(" "))' `npx lerna changed --loglevel=error | paste -d" " -s -` > lerna-scopes.txt
    - echo "Packages with changes:" && cat lerna-scopes.txt
  artifacts:
    paths:
      - lerna-scopes.txt
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH == "dev"'
    - if: '$CI_COMMIT_BRANCH == "master"'

lint:
  stage: code-quality-check
  extends:
    - .branch-cache-puller-job
    - .retrying-job
  script: npx lerna run lint --stream $(cat lerna-scopes.txt)
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

format:
  stage: code-quality-check
  extends:
    - .branch-cache-puller-job
    - .retrying-job
  script: npx lerna run format --stream $(cat lerna-scopes.txt)
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

types-check:
  stage: code-quality-check
  extends:
    - .branch-cache-puller-job
    - .retrying-job
  script: npx lerna run type-check --stream $(cat lerna-scopes.txt)
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

unit-test:
  stage: test
  extends:
    - .branch-cache-puller-job
    - .retrying-job
  script: npx lerna run test --stream $(cat lerna-scopes.txt)
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

build-types:
  stage: build
  extends:
    - .branch-cache-puller-job
    - .retrying-job
  script: npx lerna run build:types --stream $(cat lerna-scopes.txt)
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

build-code:
  stage: build
  extends:
    - .branch-cache-puller-job
    - .retrying-job
  script: npx lerna run build:code --stream $(cat lerna-scopes.txt)
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

# The build job uploads the built files as pipeline artifacts so that the release
# job does not need to build again.
build:
  stage: build
  environment:
    name: production
  extends:
    - .branch-cache-puller-job
    - .retrying-job
  script:
    - npx lerna run build --stream $(cat lerna-scopes.txt)
  artifacts:
    paths:
      - ./packages/**/dist
      - ./packages/**/coverage
  rules:
    - if: '$CI_COMMIT_BRANCH == "dev"'
    - if: '$CI_COMMIT_BRANCH == "master"'

# This job performs the actual release of the changed packages.
#
# It does the following:
#
# 1. Sets up the runner shell environment with the right Git credentials and remote.
# 2. Switches to the `master` branch.
# 3. Run lerna publish, which:
#   3.1. Identifies packages that have changed since last tag
#   3.2. Scans commit messages since last tag
#   3.3. Infers next version
#   3.4. Bumps version in package.json for each changed package
#   3.5. Updates CHANGELOG.md for each changed package
#   3.6. Commits updated files
#   3.7. Tags commit with new version
#   3.8. Pushes new commit and tag(s) to remote git repo
#   3.9. Posts new release entry/entries to GitLab
#   3.9. Publishes new packages to GitLab package registry
#
# The GITLAB_TOKEN CI/CD variable is set at the ftdr group level and inherited
# projects under it. It has sufficient access privileges to both be used for
# pushing code via the git CLI as well as to be used when calling the gitLab web API.
#
# For any commits or tags that are created as part of a pipeline job, it is
# important that the git environment variables that are used by git to mark
# author and committer on commits/tags are properly set. Without this when such
# commits/tags are pushed to GitLab it could end up not recognizing the user
# as a valid GitLab user and thus reject that push.
.release-script-common-commands:
  script:
    - !reference [.npm-config-script-commands, script]
    - export GL_TOKEN=${GITLAB_TOKEN}
    - git remote set-url origin $(node -e 'const a = process.argv[1].split("//gitlab"); a.splice(1,0,`//${process.env.CI_PROJECT_NAME}:${process.env.GITLAB_TOKEN}@gitlab`); console.log(a.join(""))' ${CI_PROJECT_URL})
    - export GIT_AUTHOR_NAME="Ftdr-Gitlab-SVC"
    - export GIT_AUTHOR_EMAIL=""ftdr-gitlab-svc@ahs.com""
    - export GIT_COMMITTER_NAME="${GIT_AUTHOR_NAME}"
    - export GIT_COMMITTER_EMAIL="${GIT_AUTHOR_EMAIL}"
    - git config --global user.name "${GIT_AUTHOR_NAME}"
    - git config --global user.email "${GIT_AUTHOR_EMAIL}"
    - git checkout ${CI_COMMIT_BRANCH}

# release candidate
prerelease:
  stage: release
  environment:
    name: production
  extends:
    - .branch-cache-puller-job
  script:
    - !reference [.release-script-common-commands, script]
    # We can either publish a proper pre-release, just like a regular release.
    # The catch in this case is that:
    # 1. the release notes / CHANGELOG get polluted by fragmented RC change entries
    # 2. the final stable release is not able to capture a consolidated release notes / CHANGELOG
    # - 'npx lerna publish --conventional-prerelease --preid rc --message "chore: publish release candidate [skip ci]" --yes'
    #
    # OR
    #
    # We can do this workaround. In this case we get a nice consolidated release notes / CHANGELOG during the eventual stable release.
    # But we loose ny trace of RX releases in the release notes / CHANGELOG. We only get a RC release commit as a kind of trace checkpoint.
    # No RC tags are present in the commit history
    - 'npx lerna version --conventional-prerelease --preid rc --message "chore: publish release candidate [skip ci]" --yes --no-changelog --no-push'
    - for t in $(git tag --points-at); do git tag -d $t; done
    - git push
    - npx lerna publish from-package --yes
  rules:
    - if: '$CI_COMMIT_BRANCH == "dev"'

# stable release
release:
  stage: release
  environment:
    name: production
  extends:
    - .branch-cache-puller-job
  script:
    - !reference [.release-script-common-commands, script]
    - 'npx lerna version --message "chore: publish [skip ci]" --yes'
    - "npm lerna run build --stream --include-dependencies"
    - "npx lerna publish from-git"
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: manual
