module.exports = {
  presets: [
    [
      "@ftdr/babel-preset",
      {
        typescript: true,
      },
    ],
  ],
};
